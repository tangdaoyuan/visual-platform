import Vue from 'vue'
import * as ElementUI from 'element-ui'
import lodash from 'lodash'
import jquery from 'jquery'
import echarts from 'echarts'
import Cookies from 'js-cookie'
import iView from 'iview'

import './sw/registerServiceWorker'

import App from './App.vue'
import router from './routers/router'
import store from './stores'
import { services } from './services'
import Components from './components'
import Utils from './utils/index'
import CONSTANT from './constants/beans'

import 'iview/dist/styles/iview.css'
import 'element-ui/lib/theme-chalk/index.css'
import './assets/sass/main.scss'

Vue.config.productionTip = false
Vue.use(ElementUI)
Vue.use(iView)
Components.init(Vue)

Vue.prototype = Object.assign(Vue.prototype, {
  ...services,
  _: lodash,
  $: jquery,
  echarts,
  cookies: Cookies,
  utils: Utils,
  CONSTANT
})

new Vue({
  router,
  store,
  render: (h) => h(App)
}).$mount('#app')
