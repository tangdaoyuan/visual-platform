import ProtectMenu from '@/assets/imgs/protect-menu.png'
import DrugsMenu from '@/assets/imgs/drugs-menu.png'
import CriminalMenu from '@/assets/imgs/criminal-menu.png'
import EconomyMenu from '@/assets/imgs/economy-menu.png'
import SupervisorMenu from '@/assets/imgs/supervisor-menu.png'
import TrafficMenu from '@/assets/imgs/traffic-menu.png'
import OverseerMenu from '@/assets/imgs/overseer-menu.png'
import SecurityMenu from '@/assets/imgs/security-menu.png'
import LawMenu from '@/assets/imgs/law-menu.png'
import ConductMenu from '@/assets/imgs/conduct-menu.png'

export class CONSTANT {
  public demandStatus: any = {
    UNCHECK: 1,
    REJECT: 2,
    DEVELOPING: 3,
    FINISH: 4,
    REVOKE: 5
  }

  public demandText: string[] = [
    '', '审核中', '未通过', '研发中', '研发完成'
  ]
  public emergencyStatus: any = {
    SPECIAL: 1,
    URGENT: 2,
    NORMAL: 3
  }
  public emergencyOptions: any = [
    {
      name: '特急',
      value: 1
    },
    {
      name: '紧急',
      value: 2
    },
    {
      name: '一般',
      value: 3
    }
  ]

  public menuSort = {
    RISE: 'rise',
    DOWN: 'down',
    TOP: 'top'
  }

  public menuIcons = [
    {
      name: '国保支队',
      icon: ProtectMenu
    },
    {
      name: '禁毒支队',
      icon: DrugsMenu
    },
    {
      name: '刑侦支队',
      icon: CriminalMenu
    },
    {
      name: '经侦支队',
      icon: EconomyMenu
    },
    {
      name: '监管支队',
      icon: SupervisorMenu
    },
    {
      name: '交管支队',
      icon: TrafficMenu
    },
    {
      name: '督查支队',
      icon: OverseerMenu
    },
    {
      name: '治安支队',
      icon: SecurityMenu
    },
    {
      name: '法制支队',
      icon: LawMenu
    },
    {
      name: '警务指挥',
      icon: ConductMenu
    }
  ]
}

export default new CONSTANT()
