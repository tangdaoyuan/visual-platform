import { Commit } from 'vuex'

class MenuStatus {
  public refreshTopMenu: boolean = false
  public refreshSubMenu: boolean = false
  public refreshScreenContent: boolean = false
}

interface State {
  menuStatus: MenuStatus
}

const state: State = {
  menuStatus: new MenuStatus()
}

const actions = {}

const getters = {
  isRefreshTopMenu (state: State) {
    return state.menuStatus.refreshTopMenu
  },
  isRefreshSubMenu (state: State) {
    return state.menuStatus.refreshSubMenu
  },
  isRefreshScrCon (state: State) {
    return state.menuStatus.refreshScreenContent
  }
}

const mutations = {
  activeTopMenu (state: State): void {
    state.menuStatus.refreshTopMenu = true
  },
  activeSubMenu (state: State): void {
    state.menuStatus.refreshSubMenu = true
  },
  activeScrCon (state: State): void {
    state.menuStatus.refreshScreenContent = true
  },
  inactiveTopMenu (state: State): void {
    state.menuStatus.refreshTopMenu = false
  },
  inactiveSubMenu (state: State): void {
    state.menuStatus.refreshSubMenu = false
  },
  inactiveScrCon (state: State): void {
    state.menuStatus.refreshScreenContent = false
  }
}

export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations
}
