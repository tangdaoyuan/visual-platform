import { Commit } from 'vuex'

interface HomeState {
  adminFileViewStatus: string
}

const state: HomeState = {
  adminFileViewStatus: 'admin'
}

const actions = {
  setAdminFileViewStatus (context: { commit: Commit; state: HomeState }, adminFileViewStatus: string): void {
    context.commit('setAdminFileViewStatus', adminFileViewStatus)
  }
}

const getters = {
  adminFileViewStatus (state: HomeState): string {
    return state.adminFileViewStatus
  }
}

const mutations = {
  setAdminFileViewStatus (state: HomeState, adminFileViewStatus: string): void {
    state.adminFileViewStatus = adminFileViewStatus
  }
}

export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations
}
