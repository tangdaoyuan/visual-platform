import { Commit } from 'vuex'
import Cookie from 'js-cookie'

interface UserState {
  isLogin: boolean,
  userInfo: any
}

const state: UserState = {
  isLogin: false,
  userInfo: null
}

const actions = {
  setUserInfo (context: { commit: Commit; state: UserState }, userInfo: any) {
    context.commit('setUserInfo', userInfo)
    Cookie.set('user_info', JSON.stringify(userInfo))
  },
  setLoginStatus (context: { commit: Commit; state: UserState }, loginStatus: boolean) {
    context.commit('setLoginStatus', loginStatus)
    if (loginStatus) {
      Cookie.set('login_status', 'login')
    } else {
      Cookie.remove('login_status')
      context.commit('setUserInfo', null)
      Cookie.remove('user_info')
    }
  }
}

const getters = {
  userInfo (state: UserState): any {
    return state.userInfo || Cookie.getJSON('user_info')
  },
  isLogin (state: UserState): boolean {
    return state.isLogin || !!Cookie.get('login_status')
  },
  isAdmin (state: UserState): boolean {
    if (state.userInfo) {
      return state.userInfo.is_admin
    }
    if (Cookie.getJSON('user_info')) {
      return Cookie.getJSON('user_info').is_admin
    }
    return false
  }
}


const mutations = {
  setUserInfo (state: UserState, userInfo: any): void {
    state.userInfo = userInfo
  },
  setLoginStatus (state: UserState, loginStatus: boolean) {
    state.isLogin = loginStatus
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}
