import { Vue, Component, Prop, Watch } from 'vue-property-decorator'
import { Action, Getter, Mutation } from 'vuex-class'
import userIcon from '@/assets/imgs/user-icon.png'

@Component
export default class Manange extends Vue {
  @Action('user/setLoginStatus') public setLoginStatus: any
  @Getter('user/userInfo') public userInfo!: any

  public activeIndex: number = 3
  public defaultIcon: string = userIcon
  private path: any = ['manage', 'roleManage', 'userManage', 'topMenu', 'materialManage']

  public handleSelect (active: number): void {
    this.activeIndex = active
    this.$router.push({
      name: this.path[active]
    })
  }

  public created (): void {
    switch (this.$route.name) {
      case this.path[0]:
      default:
        this.$router.push({
          name: this.path[3]
        })
        break
      case this.path[1]:
        this.activeIndex = 1
        break
      case this.path[2]:
        this.activeIndex = 2
        break
      case this.path[3]:
        this.activeIndex = 3
        break
      case this.path[4]:
        this.activeIndex = 4
        break
    }
  }

  public logout (): void {
    this.loginService.logout({}).then((res: any) => {
      if (res.status === 0) {
        this.setLoginStatus(false)
        this.$router.push({
          name: 'login'
        })
      } else {
        this.$message({
          message: res.msg,
          type: 'error'
        })
      }
    })
  }
}
