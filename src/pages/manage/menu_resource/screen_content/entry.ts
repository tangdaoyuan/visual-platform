import { Vue, Component, Prop, Watch } from 'vue-property-decorator'
import { Action, Getter, Mutation } from 'vuex-class'
import ScreenContentModal from '../components/screen_content_modal/index.vue'

@Component({
  components: {
    ScreenContentModal
  }
})
export default class ScreenContent extends Vue {
  @Getter('manage/isRefreshScrCon') public refreshTag !: boolean
  @Mutation('manage/inactiveScrCon') public resetFreshTag: any

  public totalCount: number = 1

  public tableData: any[] = []

  public modals: ModalGroup = new ModalGroup()

  public searchParams: SearchParams = new SearchParams()

  public menuId: string = ''

  public created (): void {
    this.refreshContent()
  }

  public edit (row: any): void {
    this.menuId = row.id
    this.modals.showScreenModal()
  }

  public remove (row: any): void {
    row.showDeleteModal = false
    this.manageService.menuService.deleteMenu({
      id: row.id
    }).then((res: any) => {
      if (res.status === 0) {
        this.refreshContent()
      } else {
        this.$message.error(res.msg)
      }
    })
  }

  public refreshContent (): void {
    this.manageService.menuService.getMenuList({
      ...this.searchParams
    }).then((res: any) => {
      if (res.status === 0) {
        this.totalCount = res.data.count
        this.tableData = res.data.list.map((item: any) => {
          return {
            ...item,
            showDeleteModal: false
          }
        })
      } else {
        this.$message.error(res.msg)
      }
    })
  }

  get isRefresh (): boolean {
    return this.refreshTag
  }

  public changePage (index: number): void {
    this.searchParams.page_no = index
    this.refreshContent()
  }

  public sortMenu (menuId: string, flag: string): void {
    this.manageService.menuService.sortMenu({
      id: menuId,
      type: flag
    }).then((res: any) => {
      if (res.status === 0) {
        this.refreshContent()
      }
    })
  }

  @Watch('isRefresh')
  public refresh (): void {
    if (this.isRefresh) {
      this.refreshContent()
      this.resetFreshTag()
    }
  }
}

class SearchParams {
  public level: number
  public page_no: number
  public page_size: number
  public search_content: string

  constructor (
    level: number = 3,
    page_no: number = 1,
    page_size: number = 10,
    search_content: string = '') {
    [
      this.level,
      this.page_no,
      this.page_size,
      this.search_content
    ] = [
      level,
      page_no,
      page_size,
      search_content
    ]
  }
}

class ModalGroup {
  public screenModal !: boolean

  constructor () {
    [
      this.screenModal
    ] = [
      false
    ]
  }

  public showScreenModal = (): void => {
    this.screenModal = true
  }

  public closeScreenModal = (): void => {
    this.screenModal = false
  }
}
