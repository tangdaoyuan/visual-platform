import { Vue, Component, Prop, Watch } from 'vue-property-decorator'
import { Action, Getter, Mutation } from 'vuex-class'
import TopMenuModal from '../components/top_menu_modal/index.vue'

@Component({
  components: {
    TopMenuModal
  }
})
export default class TopMenu extends Vue {
  @Getter('manage/isRefreshTopMenu') public refreshTag !: boolean
  @Mutation('manage/inactiveTopMenu') public resetFreshTag: any

  public totalCount: number = 1

  public tableData: any[] = []

  public modals: ModalGroup = new ModalGroup()

  public searchParams: SearchParams = new SearchParams()

  public menuId: string = ''

  public created (): void {
    this.refreshMenu()
  }

  public edit (row: any): void {
    this.menuId = row.id
    this.modals.showTopModal()
  }

  public remove (row: any): void {
    row.showDeleteModal = false
    this.manageService.menuService.deleteMenu({
      id: row.id
    }).then((res: any) => {
      if (res.status === 0) {
        this.refreshMenu()
      } else {
        this.$message.error(res.msg)
      }
    })
  }

  public refreshMenu (): void {
    this.manageService.menuService.getMenuList({
      ...this.searchParams
    }).then((res: any) => {
      if (res.status === 0) {
        this.totalCount = res.data.count
        this.tableData = res.data.list.map((item: any) => {
          return {
            ...item,
            showDeleteModal: false
          }
        })
      } else {
        this.$message.error(res.msg || '获取菜单列表失败')
      }
    })
  }


  public changePage (index: number): void {
    this.searchParams.page_no = index
    this.refreshMenu()
  }

  public sortMenu (menuId: string, flag: string): void {
    this.manageService.menuService.sortMenu({
      id: menuId,
      type: flag
    }).then((res: any) => {
      if (res.status === 0) {
        this.refreshMenu()
      }
    })
  }

  get isRefresh (): boolean {
    return this.refreshTag
  }

  @Watch('isRefresh')
  public refresh (): void {
    if (this.isRefresh) {
      this.refreshMenu()
      this.resetFreshTag()
    }
  }
}

class SearchParams {
  public level: number
  public page_no: number
  public page_size: number

  constructor (
    level: number = 1,
    page_no: number = 1,
    page_size: number = 10) {
    [
      this.level,
      this.page_no,
      this.page_size
    ] = [
      level,
      page_no,
      page_size
    ]
  }
}

class ModalGroup {
  public topModal !: boolean

  constructor () {
    [
      this.topModal
    ] = [
      false
    ]
  }

  public showTopModal = () => {
    this.topModal = true
  }

  public closeTopModal = () => {
    this.topModal = false
  }
}
