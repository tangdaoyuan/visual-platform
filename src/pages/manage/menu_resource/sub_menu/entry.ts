import { Vue, Component, Prop, Watch } from 'vue-property-decorator'
import { Action, Getter, Mutation } from 'vuex-class'
import SubMenuModal from '../components/sub_menu_modal/index.vue'

@Component({
  components: {
    SubMenuModal
  }
})
export default class SubMenu extends Vue {
  @Getter('manage/isRefreshSubMenu') public refreshTag !: boolean
  @Mutation('manage/inactiveSubMenu') public resetFreshTag: any

  public totalCount: number = 1

  public tableData: any[] = []

  public modals: ModalGroup = new ModalGroup()

  public searchParams: SearchParams = new SearchParams()

  public menuId: string = ''

  public created (): void {
    this.refreshMenu()
  }
  public edit (row: any): void {
    this.menuId = row.id
    this.showSubModal()
  }
  public remove (row: any): void {
    row.showDeleteModal = false
    this.manageService.menuService.deleteMenu({
      id: row.id
    }).then((res: any) => {
      if (res.status === 0) {
        this.refreshMenu()
      } else {
        this.$message.error(res.msg)
      }
    })
  }
  public refreshMenu (): void {
    this.manageService.menuService.getMenuList({
      ...this.searchParams
    }).then((res: any) => {
      if (res.status === 0) {
        this.totalCount = res.data.count
        this.tableData = res.data.list.map((item: any) => {
          return {
            ...item,
            showDeleteModal: false
          }
        })
      } else {
        this.$message.error(res.msg)
      }
    })
  }

  public changePage (index: number): void {
    this.searchParams.page_no = index
    this.refreshMenu()
  }

  public sortMenu (menuId: string, flag: string): void {
    this.manageService.menuService.sortMenu({
      id: menuId,
      type: flag
    }).then((res: any) => {
      if (res.status === 0) {
        this.refreshMenu()
      }
    })
  }

  get isRefresh (): boolean {
    return this.refreshTag
  }

  @Watch('isRefresh')
  public refresh (): void {
    if (this.isRefresh) {
      this.refreshMenu()
      this.resetFreshTag()
    }
  }

  public showSubModal (): void {
    this.modals.subModal = true
  }

  public closeSubModal (): void {
    this.modals.subModal = false
  }
}

class SearchParams {
  public level: number
  public page_no: number
  public page_size: number

  constructor (
    level: number = 2,
    page_no: number = 1,
    page_size: number = 10) {
    [
      this.level,
      this.page_no,
      this.page_size
    ] = [
      level,
      page_no,
      page_size
    ]
  }
}

class ModalGroup {
  public subModal !: boolean

  constructor () {
    [
      this.subModal
    ] = [
      false
    ]
  }
}
