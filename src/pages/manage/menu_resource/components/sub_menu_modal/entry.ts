import { Vue, Component, Prop, Watch, Emit } from 'vue-property-decorator'
import { Action, Getter, Mutation } from 'vuex-class'

@Component
export default class SubMenuModal extends Vue {
  public $refs !: {
    subForm: any
  }

  @Prop({
    type: Boolean,
    default: false
  }) public value !: boolean

  @Prop({
    type: String,
    default: ''
  }) public menuId !: string

  @Mutation('manage/activeSubMenu') public refreshSubMenu: any

  public saveParams: SaveParams = new SaveParams()

  public saveRule: SaveRule = new SaveRule()

  public rangeOptions: any[] = []

  public init (): void {
    this.manageService.menuService.getMenuList({
      level: 1
    }).then((res: any) => {
      if (res.status === 0) {
        this.rangeOptions = res.data.list
      } else {
        this.$message.error(res.msg || '获取菜单列表失败')
      }
    })

    if (this.menuId) {
      this.manageService.menuService.getMenuDetail({
        id: this.menuId
      }).then((res: any) => {
        if (res.status === 0) {
          const data = res.data
          this.saveParams = new SaveParams(
            data.id,
            data.parent_menu_id,
            data.name,
            data.describe
            // data.seq
          )
        } else {
          this.$message.error(res.msg)
        }
      })
    }
  }

  public confirm (): void {
    this.$refs.subForm.validate((valid: boolean, errors: any) => {
      if (valid) {
        const formData = this.utils.json2FormData({ ...this.saveParams })
        this.manageService.menuService.saveMenu(
          formData
        ).then((res: any) => {
          if (res.status === 0) {
            if (this.$route.name === 'subMenu') {
              this.refreshSubMenu()
            }
            this.close()
          } else {
            this.$message.error(res.msg)
          }
        })
      } else {
        for (const key of Object.keys(errors)) {
          this.$message.error(errors[key][0].message)
          break
        }
        return false
      }
    })
  }

  public close (): void {
    this.$emit('close')
  }

  @Watch('value')
  public visible () {
    if (this.value) {
      this.init()
    } else {
      Object.assign(this.saveParams, new SaveParams())
    }
  }
}

class SaveParams {
  public id ?: string | undefined
  public parent_menu_id !: string | undefined
  public name !: string
  public describe ?: string
  // public seq ?: number

  constructor (
    id ?: string | undefined,
    parent_menu_id ?: string | undefined,
    name: string = '',
    describe ?: string
    // seq ?: number | undefined
  ) {
    [
      this.id,
      this.parent_menu_id,
      this.name,
      this.describe
      // this.seq
    ] = [
      id,
      parent_menu_id,
      name,
      describe
      // seq
    ]
  }
}

class SaveRule {
  public name: Rule
  // public seq: Rule
  public parent_menu_id: Rule

  constructor () {
    [
      this.name,
      // this.seq,
      this.parent_menu_id
    ] = [
      new Rule('name'),
      // new Rule('seq'),
      new Rule('parent_menu_id')
    ]
  }
}

class Rule {
  public required: boolean
  public trigger: string
  public validator: any

  constructor (type: string, required: boolean = true, trigger: string = 'blur') {
    let message: string
    {
      message = (() => {
        switch (type) {
          case 'name':
            return '请输入需求名称'
          // case 'seq':
          //   return '请输入序号'
          case 'parent_menu_id':
            return '请选择二级菜单'
          default:
            return '请输入'
        }
      })()
    }
    [
      this.required,
      this.trigger,
      this.validator
    ] = [
      required,
      trigger,
      (rule: any, value: string, callback: any) => {
        if (!value) {
          callback(new Error(message))
        }
        callback()
      }
    ]
  }
}
