import { Vue, Component, Prop, Watch, Emit } from 'vue-property-decorator'
import { Action, Getter, Mutation } from 'vuex-class'
import IconModal from '../icon_modal/index.vue'

@Component({
  components: {
    IconModal
  }
})
export default class TopMenuModal extends Vue {
  public $refs !: {
    topMenuUpload: any,
    topForm: any
  }

  @Prop({
    type: Boolean,
    default: false
  }) public value !: boolean

  @Prop({
    type: String,
    default: ''
  }) public menuId !: string

  @Mutation('manage/activeTopMenu') public refreshTopMenu: any

  public saveRule: SaveRule = new SaveRule()

  public saveParams: SaveParams = new SaveParams()

  public modals: ModalGroup = new ModalGroup()

  public init (): void {
    if (this.menuId) {
      this.manageService.menuService.getMenuDetail({
        id: this.menuId
      }).then((res: any) => {
        if (res.status === 0) {
          const data = res.data
          this.saveParams = new SaveParams(
            data.id,
            data.name,
            data.describe
          )

          this.manageService.staticService.image(
            data.image
          ).then((blob: any) => {
            this.saveParams.fileList = [
              new File([blob], 'filename.jpg')
            ]
          })
        } else {
          this.$message.error(res.msg)
        }
      })
    }
  }

  public confirm (): void {
    this.$refs.topForm.validate((valid: boolean, errors: any) => {
      if (valid) {
        const formData = this.utils.json2FormData(this.saveParams.format())
        this.manageService.menuService.saveMenu(
          formData
        ).then((res: any) => {
          if (res.status === 0) {
            if (this.$route.name === 'topMenu') {
              this.refreshTopMenu()
            }
            this.close()
          } else {
            this.$message.error(res.msg)
          }
        })
      } else {
        for (const key of Object.keys(errors)) {
          this.$message.error(errors[key][0].message)
          break
        }
        return false
      }
    })
  }

  public changeUpload (file: any) {
    if (file.status === 'ready') {
      const fileType = file.raw.type
      if (!this.utils.isImgType(fileType)) {
        this.$message.error('图片格式错误')
        return
      }
      if (file.size / 1024 / 1024 > 2) {
        this.$message.error('图片超出大小限制')
        return
      }
      this.saveParams.fileList = [file.raw]
    }
  }

  public chooseIcon (data: any): void {
    this.saveParams.fileList = [
      new File([data.data], data.name)
    ]
  }

  public close (): void {
    this.$emit('close')
  }

  @Watch('value')
  public visible () {
    if (this.value) {
      this.init()
    } else {
      Object.assign(this.saveParams, new SaveParams())
    }
  }
}

class SaveParams {
  public id ?: string | undefined
  public name !: string
  public describe ?: string
  public fileList !: any[]

  constructor (
    id ?: string | undefined,
    name: string = '',
    describe: string = '',
    fileList: any[] = []
  ) {
    [
      this.id,
      this.name,
      this.fileList,
      this.describe
    ] = [
      id,
      name,
      fileList,
      describe
    ]
  }

  public format (): any {
    return {
      ...this,
      fileList: undefined,
      upload: this.fileList[0]
    }
  }

  public get fileName (): string {
    if (this.fileList.length > 0) {
      return this.fileList[0].name
    }
    return '请选择图标'
  }

  public get src (): string {
    if (this.fileList.length <= 0) return ''
    return URL.createObjectURL(this.fileList[0])
  }
}

class SaveRule {
  public name: Rule

  constructor () {
    [
      this.name
    ] = [
      new Rule('name')
    ]
  }
}

class Rule {
  public required: boolean
  public trigger: string
  public validator: any

  constructor (type: string, required: boolean = true, trigger: string = 'blur') {
    let message: string
    {
      message = (() => {
        switch (type) {
          case 'name':
            return '请输入需求名称'
          default:
            return '请输入'
        }
      })()
    }
    [
      this.required,
      this.trigger,
      this.validator
    ] = [
      required,
      trigger,
      (rule: any, value: string, callback: any) => {
        if (!value) {
          callback(new Error(message))
        }
        callback()
      }
    ]
  }
}
class ModalGroup {
  public iconModal!: boolean

  constructor () {
    [
      this.iconModal
    ] = [
      false
    ]
  }

  public showIconModal = (e: any) => {
    e.stopPropagation()
    this.iconModal = true
  }

  public closeIconModal = (e: any) => {
    if (e) e.stopPropagation()
    this.iconModal = false
  }
}
