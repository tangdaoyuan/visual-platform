import { Vue, Component, Prop, Watch, Emit } from 'vue-property-decorator'
import { Action, Getter, Mutation } from 'vuex-class'
import IconModal from '../icon_modal/index.vue'

@Component({
  components: {
    IconModal
  }
})
export default class ScreenContentModal extends Vue {
  public $refs !: {
    scrConUpload: any,
    screenForm: any
  }

  @Prop({
    type: Boolean,
    default: false
  }) public value !: boolean

  @Prop({
    type: String,
    default: ''
  }) public menuId !: string

  @Mutation('manage/activeScrCon') public refreshContent: any

  public saveParams: SaveParams = new SaveParams()

  public saveRule: SaveRule = new SaveRule()

  public modals: ModalGroup = new ModalGroup()

  public rangeOptions: any[] = []

  public subRangeOptions: any[] = []

  public demandList: any[] = []

  public init (): void {
    this.manageService.menuService.getMenuList({
      level: 1
    }).then((res: any) => {
      if (res.status === 0) {
        this.rangeOptions = res.data.list
      } else {
        this.$message.error(res.msg || '获取菜单列表失败')
      }
    })
    this.manageService.demandService.getDemandList({})
      .then((res: any) => {
        if (res.status === 0) {
          this.demandList = res.data.list
        } else {
          this.$message.error(res.msg || '获取需求列表失败')
        }
      })
    if (this.menuId) {
      this.manageService.menuService.getMenuDetail({
        id: this.menuId
      }).then((res: any) => {
        if (res.status === 0) {
          const data = res.data
          this.saveParams = new SaveParams(
            data.id,
            data.parent_menu_id,
            data.top_parent_id,
            data.name,
            data.describe,
            // data.seq,
            data.resource_url,
            data.needs_id
          )
          this.getSubOptions(data.top_parent_id)
          this.manageService.staticService.image(
            data.image
          ).then((blob: any) => {
            this.saveParams.fileList = [
              new File([blob], 'filename.jpg')
            ]
          })
        } else {
          this.$message.error(res.msg)
        }
      })
    }
  }

  public getSubOptions (id: string): void {
    this.manageService.menuService.getMenuList({
      level: 2,
      parent_menu_id: id
    }).then((res: any) => {
      if (res.status === 0) {
        this.subRangeOptions = res.data.list
      } else {
        this.$message.error(res.msg || '获取菜单列表失败')
      }
    })
  }

  public confirm (): void {
    this.$refs.screenForm.validate((valid: boolean, errors: any) => {
      if (valid) {
        const formData = this.utils.json2FormData(this.saveParams.format())
        this.manageService.menuService.saveMenu(
          formData
        ).then((res: any) => {
          if (res.status === 0) {
            if (this.$route.name === 'screenContent') {
              this.refreshContent()
            }
            this.close()
          } else {
            this.$message.error(res.msg)
          }
        })
      } else {
        for (const key of Object.keys(errors)) {
          this.$message.error(errors[key][0].message)
          break
        }
        return false
      }
    })

  }

  public close (): void {
    this.$emit('close')
  }

  public changeUpload (file: any) {
    if (file.status === 'ready') {
      const fileType = file.raw.type
      if (!this.utils.isImgType(fileType)) {
        this.$message.error('图片格式错误')
        return
      }
      if (file.size / 1024 / 1024 > 2) {
        this.$message.error('图片超出大小限制')
        return
      }
      this.saveParams.fileList = [file.raw]
    }
  }

  public chooseIcon (data: any): void {
    this.saveParams.fileList = [
      new File([data.data], data.name)
    ]
  }

  public chooseTopParent (id: string) {
    this.subRangeOptions = []
    this.saveParams.parent_menu_id = ''
    this.getSubOptions(id)
  }

  @Watch('value')
  public visible () {
    if (this.value) {
      this.init()
    } else {
      Object.assign(this.saveParams, new SaveParams())
      // this.$refs.scrConUpload.clearFiles()
    }
  }

}

class SaveParams {
  public id ?: string | undefined
  public parent_menu_id !: string | undefined
  public top_parent_id !: string | undefined
  public name !: string
  public describe ?: string
  // public seq ?: number
  public resource_url ?: string
  public needs_id: string
  public fileList !: any[]

  constructor (
    id ?: string | undefined,
    parent_menu_id ?: string | undefined,
    top_parent_id ?: string | undefined,
    name: string = '',
    describe: string = '',
    // seq ?: number | undefined,
    resource_url: string = '',
    needs_id: string = '',
    fileList: any[] = []
  ) {
    [
      this.id,
      this.parent_menu_id,
      this.top_parent_id,
      this.name,
      this.describe,
      // this.seq,
      this.resource_url,
      this.needs_id,
      this.fileList
    ] = [
      id,
      parent_menu_id,
      top_parent_id,
      name,
      describe,
      // seq,
      resource_url,
      needs_id,
      fileList
    ]
  }

  public format (): any {
    return {
      ...this,
      fileList: undefined,
      imgSrc: undefined,
      upload: this.fileList[0]
    }
  }

  public get fileName (): string {
    if (this.fileList.length > 0) {
      return this.fileList[0].name
    }
    return '请选择图标'
  }

  public get src (): string {
    if (this.fileList.length <= 0) return ''
    return URL.createObjectURL(this.fileList[0])
  }
}

class SaveRule {
  public name: Rule
  // public seq: Rule
  // public needs_id: Rule
  public parent_menu_id: Rule
  public top_parent_id: Rule
  public resource_url: Rule

  constructor () {
    [
      this.name,
      // this.seq,
      // this.needs_id,
      this.parent_menu_id,
      this.top_parent_id,
      this.resource_url
    ] = [
      new Rule('name'),
      // new Rule('seq'),
      // new Rule('needs_id'),
      new Rule('parent_menu_id'),
      new Rule('top_parent_id'),
      new Rule('resource_url')
    ]
  }
}

class Rule {
  public required: boolean
  public trigger: string
  public validator: any

  constructor (type: string, required: boolean = true, trigger: string = 'blur') {
    let message: string
    {
      message = (() => {
        switch (type) {
          case 'name':
            return '请输入需求名称'
          // case 'seq':
          //   return '请输入序号'
          case 'needs_id':
            return '请选择需求单'
          case 'top_parent_id':
            return '请选择一级菜单'
          case 'parent_menu_id':
            return '请选择二级菜单'
          case 'resource_url':
            return '请输入资源URL'
          default:
            return '请输入'
        }
      })()
    }
    [
      this.required,
      this.trigger,
      this.validator
    ] = [
      required,
      trigger,
      (rule: any, value: string, callback: any) => {
        if (!value) {
          callback(new Error(message))
        }
        callback()
      }
    ]
  }
}

class ModalGroup {
  public iconModal!: boolean

  constructor () {
    [
      this.iconModal
    ] = [
      false
    ]
  }

  public showIconModal = (e: any) => {
    e.stopPropagation()
    this.iconModal = true
  }

  public closeIconModal = (e: any) => {
    if (e) e.stopPropagation()
    this.iconModal = false
  }
}

