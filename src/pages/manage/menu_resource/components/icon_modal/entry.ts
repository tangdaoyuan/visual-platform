import { Vue, Component, Prop, Watch, Emit } from 'vue-property-decorator'
import { Action, Getter, Mutation } from 'vuex-class'

@Component
export default class IconModal extends Vue {
  public $refs !: {
    content: any
  }

  @Prop({
    type: Boolean,
    default: false
  }) public value!: boolean

  public icons: any[] = this.CONSTANT.menuIcons

  public init (): void {
    this.manageService.materialService
    .icons({})
    .then((res: ResponseProps) => {
      if (res.status === 0) {
        this.icons = res.data.map((item: any) => {
          return {
            ...item,
            icon: item.url,
            name: item.name.split('.')[0]
          }
        })
      } else {
        this.$message.error(res.msg || '获取图标列表失败')
        this.icons = this.CONSTANT.menuIcons
      }
    })
  }

  public close (): void {
    this.$emit('close')
  }

  public chooseIcon (icon: any): void {
    this.manageService.staticService
      .image(icon.icon)
      .then((blob: Blob) => {
        if (blob) {
          this.$emit('choose', { data: blob, name: icon.name })
        } else {
          this.$message.error(`本地资源${icon.icon}获取失败`)
        }
      })
  }

  @Watch('value')
  public visible () {
    if (this.value) {
      this.init()
    } else {
      console.log('close modal')
    }
  }
}
