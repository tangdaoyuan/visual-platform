import { Vue, Component, Prop, Watch, Emit } from 'vue-property-decorator'
import { Action, Getter, Mutation } from 'vuex-class'

@Component
export default class RejectModal extends Vue {
  public $refs !: {
    rejectForm: any
  }

  @Prop({
    type: Boolean,
    default: false
  }) public value !: boolean

  @Prop({
    type: String,
    default: ''
  }) public demandId !: string

  public tableData: any[] = []

  public init () {
    if (this.demandId) {
      this.manageService.demandService.getDemandDetail({
        id: this.demandId
      }).then((res: any) => {
        if (res.status === 0) {
          this.tableData = res.data.reject_record_list
        } else {
          this.$message.error(res.msg || '')
        }
      })
    }
  }


  public close (): void {
    this.$emit('close')
  }

  @Watch('value')
  public visible () {
    if (this.value) {
      this.init()
    }
  }
}
