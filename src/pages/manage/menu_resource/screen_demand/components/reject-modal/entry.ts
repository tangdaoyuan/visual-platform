import { Vue, Component, Prop, Watch, Emit } from 'vue-property-decorator'
import { Action, Getter, Mutation } from 'vuex-class'

@Component
export default class RejectModal extends Vue {
  public $refs !: {
    rejectForm: any
  }

  @Prop({
    type: Boolean,
    default: false
  }) public value !: boolean

  @Prop({
    type: String,
    default: ''
  }) public demandId !: string

  public saveParams: SaveParams = new SaveParams()

  public saveRules: SaveRule = new SaveRule()

  public init () {
    if (this.demandId) {
      this.saveParams = new SaveParams(
        this.demandId,
        this.CONSTANT.demandStatus.REJECT)
    }
  }

  public confirm () {
    this.$refs.rejectForm.validate((valid: boolean, errors: any) => {
      if (valid) {
        this.manageService.demandService.updateDemand({
          ...this.saveParams
        }).then((res: any) => {
          if (res.status === 0) {
            this.$emit('refresh')
            this.close()
          } else {
            this.$message.error(res.msg)
          }
        })
      } else {
        for (const key of Object.keys(errors)) {
          this.$message.error(errors[key][0].message)
          break
        }
        return false
      }
    })
  }

  public close (): void {
    this.$emit('close')
  }

  @Watch('value')
  public visible () {
    if (this.value) {
      this.init()
    } else {
      Object.assign(this.saveParams, new SaveParams())
    }
  }
}

class SaveParams {
  public id ?: string
  public status ?: number
  public reason ?: string

  constructor (
    id ?: string,
    status ?: number,
    reason ?: string | undefined
  ) {
    [
      this.id,
      this.status,
      this.reason
    ] = [
      id,
      status,
      reason
    ]
  }
}

class SaveRule {
  public reason: Rule

  constructor () {
    [
      this.reason
    ] = [
      new Rule('reason')
    ]
  }
}

class Rule {
  public required: boolean
  public trigger: string
  public validator: any

  constructor (type: string, required: boolean = true, trigger: string = 'blur') {
    let message: string
    {
      message = (() => {
        switch (type) {
          case 'reason':
            return '请输入驳回理由'
          default:
            return '请输入'
        }
      })()
    }
    [
      this.required,
      this.trigger,
      this.validator
    ] = [
      required,
      trigger,
      (rule: any, value: string, callback: any) => {
        if (!value) {
          callback(new Error(message))
        }
        callback()
      }
    ]
  }
}

