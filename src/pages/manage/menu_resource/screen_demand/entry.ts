import { Vue, Component, Prop } from 'vue-property-decorator'
import { Action, Getter, Mutation } from 'vuex-class'
import RejectModal from './components/reject-modal/index.vue'
import ReasonModal from './components/reason-modal/index.vue'

@Component({
  components: {
    RejectModal,
    ReasonModal
  }
})
export default class ScreenDemand extends Vue {
  public totalCount: number = 1

  public searchParams: SearchParams = new SearchParams()

  public rangeSelected: string = '默认分组'

  public rangeOptions: any[] = []

  public demandId: string = ''

  public modals: ModalGroup = new ModalGroup()

  public tableData: any[] = []

  public created (): void {
    this.search()
  }

  public pass (row: any): void {
    this.manageService.demandService.updateDemand({
      id: row.id,
      status: this.CONSTANT.demandStatus.DEVELOPING
    }).then((res: any) => {
      if (res.status === 0) {
        this.search()
      } else {
        this.$message.error(res.msg)
      }
    })
  }

  public reject (row: any): void {
    this.demandId = row.id
    this.modals.showRejectModal()
  }

  public reason (row: any): void {
    this.demandId = row.id
    this.modals.showReasonModal()
  }

  public filterChange (val: number): void {
    console.log(val)
  }

  public search (): void {
    this.manageService.demandService.getDemandList({
      ...this.searchParams
    }).then((res: any) => {
      if (res.status === 0) {
        this.totalCount = res.data.count
        this.tableData = res.data.list
      } else {
        this.$message.error(res.msg)
      }
    })
  }

  public changePage (index: number): void {
    this.searchParams.page_no = index
    this.search()
  }

  public closeRejectModal (): void {
    this.modals.rejectModal = false
  }
}

class SearchParams {
  public keyword: string
  public page_no: number
  public page_size: number

  constructor (
    keyword: string = '',
    page_no: number = 1,
    page_size: number = 10) {
    [
      this.keyword,
      this.page_no,
      this.page_size
    ] = [
      keyword,
      page_no,
      page_size
    ]
  }
}

class ModalGroup {
  public rejectModal: boolean
  public reasonModal: boolean

  constructor () {
    [
      this.rejectModal,
      this.reasonModal
    ] = [
      false,
      false
    ]
  }

  public showRejectModal = (): void => {
    this.rejectModal = true
  }

  public closeRejectModal = (): void => {
    this.rejectModal = false
  }

  public showReasonModal = (): void => {
    this.reasonModal = true
  }

  public closeReasonModal = (): void => {
    this.reasonModal = false
  }
}
