import { Vue, Component, Prop } from 'vue-property-decorator'
import { Action, Getter, State } from 'vuex-class'
import TopMenuModal from './components/top_menu_modal/index.vue'
import SubMenuModal from './components/sub_menu_modal/index.vue'
import ScreenContentModal from './components/screen_content_modal/index.vue'
@Component({
  components: {
    TopMenuModal,
    SubMenuModal,
    ScreenContentModal
  }
})
export default class MenuResourceManage extends Vue {
  public menuIndex: number = 1

  public modals: ModalGroup = new ModalGroup()

  public menuSelect (key: number): void {
    this.menuIndex = key
    let target = ''
    switch (`${key}`) {
      case '1':
        target = 'topMenu'
        break
      case '2':
        target = 'subMenu'
        break
      case '3':
        target = 'screenContent'
        break
      case '4':
        target = 'screenDemand'
        break
    }
    this.$router.push({
      name: target
    })
  }
}

class ModalGroup {
  public topModal !: boolean
  public subModal !: boolean
  public screenModal !: boolean

  constructor () {
    [
      this.topModal,
      this.subModal,
      this.screenModal
    ] = [
      false,
      false,
      false
    ]
  }

  public showTopModal = (): void => {
    this.topModal = true
  }

  public closeTopModal = (): void => {
    this.topModal = false
  }

  public showSubModal = (): void => {
    this.subModal = true
  }

  public closeSubModal = (): void => {
    this.subModal = false
  }

  public showScreenModal = (): void => {
    this.screenModal = true
  }

  public closeScreenModal = (): void => {
    this.screenModal = false
  }
}
