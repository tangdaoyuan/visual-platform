import { Vue, Component, Prop, Watch, Emit } from 'vue-property-decorator'
import { Action, Getter, State } from 'vuex-class'

@Component
export default class UserModal extends Vue {
  public $refs !: {
    userForm: any
  }

  @Prop({
    type: Boolean,
    default: false
  }) public value !: boolean

  @Prop({
    type: String,
    default: ''
  }) public userId !: string

  public saveParams: SaveParams = new SaveParams()

  public saveRule: SaveRule = new SaveRule()

  public rangeOptions: any[] = []

  public init (): void {
    this.manageService.roleService.getRoleList({}).then((res: any) => {
      if (res.status === 0) {
        this.rangeOptions = res.data.list
      } else {
        this.$message.error(res.msg || '获取角色列表失败')
      }
    })

    if (this.userId) {
      this.manageService.userService.getUserDetail({
        id: this.userId
      }).then((res: any) => {
        if (res.status === 0) {
          [
            this.saveParams.id,
            this.saveParams.name,
            this.saveParams.username,
            this.saveParams.password,
            this.saveParams.role_id_list,
            this.saveParams.identity,
            this.saveParams.police_id
          ] = [
            res.data.id,
            res.data.name,
            res.data.username,
            res.data.password,
            res.data.role_id_list,
            res.data.identity,
            res.data.police_id
          ]
        } else {
          this.$message.error(res.msg || '获取用户详情失败')
        }
      })
    }
  }

  public close (): void {
    this.$emit('close')
  }

  public refresh (): void {
    this.$emit('refresh')
  }

  @Emit('refresh')
  public confirm (): void {
    this.$refs.userForm.validate((valid: boolean, errors: any) => {
      if (valid) {
        if (!this.userId) {
          this.manageService.userService
            .createUser({ ...this.saveParams })
            .then((res: any) => {
              if (res.status === 0) {
                this.refresh()
                this.close()
                this.$message.success('用户创建完成')
              } else {
                this.$message.error(res.msg || '用户创建失败')
              }
            })
        } else {
          this.manageService.userService
            .updateUser({ ...this.saveParams })
            .then((res: any) => {
              if (res.status === 0) {
                this.refresh()
                this.close()
                this.$message.success('用户更新完成')
              } else {
                this.$message.error(res.msg || '用户更新失败')
              }
            })
        }
      } else {
        for (const key of Object.keys(errors)) {
          this.$message.error(errors[key][0].message)
          break
        }
        return false
      }
    })
  }

  @Watch('value')
  public visible () {
    if (this.value) {
      this.init()
    } else {
      Object.assign(this.saveParams, new SaveParams())
    }
  }
}
class SaveParams {
  public id ?: string | undefined
  public name: string
  public username: string
  public password: string
  public role_id_list: string[]
  public identity: string
  public police_id: string

  constructor (
    id ?: string | undefined,
    name: string = '',
    username: string = '',
    password: string = '',
    role_id_list: string[] = [],
    identity: string = '',
    police_id: string = ''
  ) {
    [
      this.id,
      this.name,
      this.username,
      this.password,
      this.role_id_list,
      this.identity,
      this.police_id
    ] = [
      id,
      name,
      username,
      password,
      role_id_list,
      identity,
      police_id
    ]
  }
}

class SaveRule {
  public name: Rule
  public username: Rule
  public password: Rule
  public role_id_list: Rule
  public identity: Rule
  public police_id: Rule

  constructor () {
    [
      this.name,
      this.username,
      this.password,
      this.role_id_list,
      this.identity,
      this.police_id
    ] = [
      new Rule('name'),
      new Rule('username'),
      new Rule('password'),
      new Rule('role_id_list'),
      new Rule('identity'),
      new Rule('police_id')
    ]
  }
}

class Rule {
  public required: boolean
  public trigger: string
  public validator: any

  constructor (type: string, required: boolean = true, trigger: string = 'blur') {
    let message: string
    {
      message = (() => {
        switch (type) {
          case 'name':
            return '请输入昵称'
          case 'username':
            return '请输入用户名'
          case 'password':
            return '请输入密码'
          case 'role_id_list':
            return '请选择角色'
          case 'identity':
            return '请输入身份证号'
          case 'police_id':
            return '请输入警号'
          default:
            return '请输入'
        }
      })()
    }
    [
      this.required,
      this.trigger,
      this.validator
    ] = [
      required,
      trigger,
      (rule: any, value: any, callback: any) => {
        if (!value) {
          callback(new Error(message))
        }
        if (type === 'role_id_list' && value.length <= 0) {
          callback(new Error(message))
        }
        callback()
      }
    ]
  }
}
