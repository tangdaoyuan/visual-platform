import { Vue, Component, Prop } from 'vue-property-decorator'
import { Action, Getter, Mutation } from 'vuex-class'
import UserModal from './components/user_modal/index.vue'

@Component({
  components: {
    UserModal
  }
})
export default class UserManage extends Vue {
  public searchParams: SearchParams = new SearchParams()

  public userId: string = ''

  public tableData: any[] = []

  public modals: ModalGroup = new ModalGroup()

  public totalCount: number = 1

  public created (): void {
    this.refresh()
  }

  public refresh (): void {
    this.manageService.userService.getUserList({
      ...this.searchParams
    })
      .then((res: any) => {
        if (res.status === 0) {
          this.totalCount = res.data.total
          this.tableData = res.data.list.map((item: any) => {
            return {
              ...item,
              role_names: item.role_name_list.join('  '),
              showDelPopover: false,
              showResetPopover: false
            }
          })
        } else {
          this.$message.error(res.msg)
        }
      })
  }

  public add (): void {
    this.userId = ''
    this.modals.showUserModal()
  }
  public edit (row: any): void {
    this.userId = row.id
    this.modals.showUserModal()
  }

  public remove (row: any): void {
    row.showDelPopover = false
    this.manageService.userService.removeUser({
      id: row.id
    }).then((res: any) => {
      if (res.status === 0) {
        this.refresh()
      } else {
        this.$message.error(res.msg || '删除用户失败')
      }
    })
  }


  public resetPwd (row: any): void {
    row.showResetPopover = false
    this.manageService.userService.resetPwd({
      user_id: row.id
    }).then((res: any) => {
      if (res.status === 0) {
        this.$message.success(`用户 ${row.name} 密码重置成功`)
      } else {
        this.$message.error(res.msg || '重置密码失败')
      }
    })
  }

  public changePage (index: number): void {
    this.searchParams.page_no = index
    this.refresh()
  }
}

class SearchParams {
  public search_content: string
  public page_no: number
  public page_size: number

  constructor (
    search_content: string = '',
    page_no: number = 1,
    page_size: number = 10) {
    [
      this.search_content,
      this.page_no,
      this.page_size
    ] = [
      search_content,
      page_no,
      page_size
    ]
  }
}

class ModalGroup {
  public userModal !: boolean

  constructor () {
    [
      this.userModal
    ] = [
      false
    ]
  }

  public showUserModal = () => {
    this.userModal = true
  }

  public closeUserModal = () => {
    this.userModal = false
  }
}
