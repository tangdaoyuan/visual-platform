import { Vue, Component, Prop } from 'vue-property-decorator'
import { Action, Getter, Mutation } from 'vuex-class'
import AuthModal from './components/auth_modal/index.vue'
import GroupModal from './components/group_modal/index.vue'
import RoleModal from './components/role_modal/index.vue'

@Component({
  components: {
    AuthModal,
    GroupModal,
    RoleModal
  }
})
export default class RoleManage extends Vue {
  public totalCount: number = 1

  public searchParams: SearchParams = new SearchParams()

  public tableData: any[] = []

  public modals: ModalGroup = new ModalGroup()

  public roleId: string = ''

  public created (): void {
    this.refresh()
  }

  public refresh (): void {
    this.manageService.roleService.getRoleList({
      ...this.searchParams
    }).then((res: any) => {
      if (res.status === 0) {
        this.totalCount = res.data.count
        this.tableData = res.data.list.map((item: any) => {
          return {
            ...item,
            showDelPopover: false
          }
        })
      } else {
        this.$message.error(res.msg || '获取角色列表失败')
      }
    })
  }
  public createRole (): void {
    this.roleId = ''
    this.modals.showRoleModal()
  }

  public edit (row: any): void {
    this.roleId = row.id
    this.modals.showRoleModal()
  }

  public auth (row: any): void {
    this.roleId = row.id
    this.modals.showAuthModal()
  }

  public group (row: any): void {
    this.roleId = row.id
    this.modals.showGroupModal()
  }

  public remove (row: any): void {
    this.manageService.roleService.deleteRole({
      id: row.id
    }).then((res: any) => {
      if (res.status === 0) {
        this.refresh()
      } else {
        this.$message.error(res.msg)
      }
    })
  }

  public changePage (index: number): void {
    this.searchParams.page_no = index
    this.refresh()
  }
}

class SearchParams {
  public search_content: string
  public page_no: number
  public page_size: number

  constructor (
    search_content: string = '',
    page_no: number = 1,
    page_size: number = 10) {
    [
      this.search_content,
      this.page_no,
      this.page_size
    ] = [
      search_content,
      page_no,
      page_size
    ]
  }
}

class ModalGroup {
  public roleModal !: boolean
  public authModal !: boolean
  public groupModal !: boolean

  constructor () {
    [
      this.roleModal,
      this.authModal,
      this.groupModal
    ] = [
      false,
      false,
      false
    ]
  }

  public showRoleModal = (): void => {
    this.roleModal = true
  }

  public closeRoleModal = (): void => {
    this.roleModal = false
  }

  public showAuthModal = (): void => {
    this.authModal = true
  }

  public closeAuthModal = (): void => {
    this.authModal = false
  }

  public showGroupModal = (): void => {
    this.groupModal = true
  }

  public closeGroupModal = (): void => {
    this.groupModal = false
  }
}
