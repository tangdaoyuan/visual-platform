import { Vue, Component, Prop, Watch, Emit } from 'vue-property-decorator'
import { Action, Getter, State } from 'vuex-class'

@Component
export default class GroupModal extends Vue {
  @Prop({
    type: Boolean,
    default: false
  }) public value !: boolean

  @Prop({
    type: String,
    default: ''
  }) public roleId !: string

  public users: object[] = []

  public saveParams !: SaveParams

  public init (): void {
    if (this.roleId) {
      this.manageService.roleService.getRoleDetail({
        id: this.roleId
      }).then((res: any) => {
        if (res.status === 0) {
          const data = res.data
          this.saveParams = new SaveParams(data.id, data.user_id_list)
          this.manageService.userService.getUserList({})
            .then((res: any) => {
              if (res.status === 0) {
                this.users = res.data.map((item: any) => {
                  return {
                    ...item,
                    isChecked: this.isInUserList(item)
                  }
                })
              } else {
                this.$message.error(res.msg)
              }
            })
        } else {
          this.$message.error(res.msg)
        }
      })
    }
  }

  private isInUserList (item: any): boolean {
    for (const uid of this.saveParams.user_id_list) {
      if (uid === item.id) {
        return true
      }
    }
    return false
  }

  public confirm (): void {
    const list: string[] = []
    this.users.forEach((user: any) => {
      if (user.isChecked) {
        list.push(user.id)
      }
    })
    this.saveParams.user_id_list = list
    this.manageService.roleService
      .saveRole2User(this.saveParams.format())
      .then((res: any) => {
        if (res.status === 0) {
          this.close()
        } else {
          this.$message.error(res.msg)
        }
      })
  }

  public close (): void {
    this.$emit('close')
  }

  @Watch('value')
  public visible (): void {
    if (this.value) {
      this.init()
    } else {
      this.users = []
      Object.assign(this.saveParams, new SaveParams())
    }
  }
}

class SaveParams {
  public id !: string
  public user_id_list !: string[]

  constructor (id: string = '', user_id_list: string[] = []) {
    [
      this.id,
      this.user_id_list
    ] = [
      id,
      user_id_list
    ]
  }

  public format (): any {
    return {
      ...this
    }
  }
}
