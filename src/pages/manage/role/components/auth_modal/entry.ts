import { Vue, Component, Prop, Watch } from 'vue-property-decorator'
import { Action, Getter, State } from 'vuex-class'

@Component
export default class AuthModal extends Vue {
  public $refs !: {
    authTree: any
  }

  @Prop({
    type: Boolean,
    default: false
  }) public value !: boolean

  @Prop({
    type: String,
    default: ''
  }) public roleId !: string

  public treeProps: TreeProp = new TreeProp()

  public data: any = []

  public saveParams: SaveParams = new SaveParams()

  public init (): void {
    if (this.roleId) {
      this.manageService.roleService.getRoleDetail({
        id: this.roleId
      }).then((res: any) => {
        if (res.status === 0) {
          const data = res.data
          this.saveParams.id = data.id
          this.saveParams.menu_id_list = data.menu_id_list
        } else {
          this.$message.error(res.msg)
        }
      })
    }
    this.manageService.menuService.getMenuTreeList({}).then((res: any) => {
      if (res.status === 0) {
        // this.data = res.data
        this.data = this.utils.recursiveMap(res.data, 1)
      } else {
        this.$message.error(res.msg)
      }
    })
  }

  public confirm (): void {
    const menu_id_list: string[] = []
    this.$refs.authTree.getCheckedNodes()
      .forEach((item: any) => {
        menu_id_list.push(item.id)
      })
    this.saveParams.menu_id_list = menu_id_list
    this.manageService.roleService
      .saveRole2Menu(this.saveParams.format())
      .then((res: any) => {
        if (res.status === 0) {
          this.close()
        } else {
          this.$message.error(res.msg)
        }
      })
  }

  public checkNode (data: any, tree: any): void {
    const checked = tree.checkedKeys.indexOf(data.id) > -1 ? true : false
    if (data.level === 3 && checked) {
      if (tree.checkedKeys.indexOf(data.parent_menu_id) < 0) {
        this.$refs.authTree.setChecked(data.parent_menu_id, true)
      }
    } else if (data.level === 2 && !checked) {
      data.children.forEach((child: any) => {
        this.$refs.authTree.setChecked(child.id, false)
      })
    }
  }

  public close (): void {
    this.$emit('close')
  }

  @Watch('value')
  public visible () {
    if (this.value) {
      this.init()
    } else {
      Object.assign(this.saveParams, new SaveParams())
      this.data = []
      this.$refs.authTree.setCheckedKeys([])
    }
  }
}

class SaveParams {
  public id ?: string
  public menu_id_list ?: any[]

  constructor (id: string = '', menu_id_list: any[] = []) {
    [
      this.id,
      this.menu_id_list
    ] = [
      id,
      menu_id_list
    ]
  }
  public format (): any {
    return {
      ...this
    }
  }
}
class TreeProp {
  public label: string
  public children: string

  constructor (label: string = 'name', children: string = 'children') {
    [
      this.label,
      this.children
    ] = [
      label,
      children
    ]
  }
}
