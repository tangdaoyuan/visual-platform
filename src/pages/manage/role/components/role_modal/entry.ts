import { Vue, Component, Prop, Watch, Emit } from 'vue-property-decorator'
import { Action, Getter, State } from 'vuex-class'

@Component
export default class RoleModal extends Vue {
  public $refs !: {
    roleForm: any
  }

  @Prop({
    type: Boolean,
    default: false
  }) public value !: boolean

  @Prop({
    type: String,
    default: ''
  }) public roleId !: string

  public saveParams: SaveParams = new SaveParams()

  public saveRule: SaveRule = new SaveRule()

  public init (): void {
    if (this.roleId) {
      this.manageService.roleService.getRoleDetail({
        id: this.roleId
      }).then((res: any) => {
        if (res.status === 0) {
          const data = res.data
          this.saveParams = new SaveParams(
            data.id,
            data.name,
            data.describe
          )
        } else {
          this.$message.error(res.msg)
        }
      })
    }
  }

  public close (): void {
    this.$emit('close')
  }

  @Emit('refresh')
  public confirm (): void {
    this.$refs.roleForm.validate((valid: boolean, errors: any) => {
      if (valid) {
        this.manageService.roleService.saveRole({
          ...this.saveParams
        }).then((res: any) => {
          if (res.status === 0) {
            this.close()
          } else {
            this.$message.error(res.msg)
          }
        })
      } else {
        for (const key of Object.keys(errors)) {
          this.$message.error(errors[key][0].message)
          break
        }
        return false
      }
    })
  }

  @Watch('value')
  public visible () {
    if (this.value) {
      this.init()
    } else {
      Object.assign(this.saveParams, new SaveParams())
    }
  }
}
class SaveParams {
  public id ?: string | undefined
  public name!: string
  public describe ?: string
  public status!: number

  constructor (
    id ?: string | undefined,
    name: string = '',
    describe: string = '',
    status: number = 1
  ) {
    [
      this.id,
      this.name,
      this.describe,
      this.status
    ] = [
      id,
      name,
      describe,
      status
    ]
  }
}

class SaveRule {
  public name: Rule
  public describe: Rule

  constructor () {
    [
      this.name,
      this.describe
    ] = [
      new Rule('name'),
      new Rule('describe')
    ]
  }
}

class Rule {
  public required: boolean
  public trigger: string
  public validator: any

  constructor (type: string, required: boolean = true, trigger: string = 'blur') {
    let message: string
    {
      message = (() => {
        switch (type) {
          case 'name':
            return '请输入需求名称'
          case 'describe':
            return '请输入描述'
          default:
            return '请输入'
        }
      })()
    }
    [
      this.required,
      this.trigger,
      this.validator
    ] = [
      required,
      trigger,
      (rule: any, value: string, callback: any) => {
        if (!value) {
          callback(new Error(message))
        }
        callback()
      }
    ]
  }
}
