import { Vue, Component, Prop } from 'vue-property-decorator'
import { Action, Getter, Mutation } from 'vuex-class'

@Component({
  components: {}
})
export default class RoleManage extends Vue {
  public $refs !: {
    materialUpload: any
  }

  public searchParams: SearchParams = new SearchParams()

  public icons: any[] = []

  public lock: boolean = false

  public editingName: string = ''

  public created (): void {
    this.refresh()
  }

  public refresh (): void {
    this.manageService.materialService
      .icons({})
      .then((res: ResponseProps) => {
        if (res.status === 0) {
          this.icons = res.data.map((item: any) => {
            return {
              ...item,
              name: item.name.split('.')[0],
              isChecked: false,
              isEdit: false
            }
          })
        } else {
          this.$message.error(res.msg || '获取图标列表失败')
        }
      })
  }

  public removeIcons () {
    const dels = this.icons.filter((item: any) => {
      return item.isChecked ? true : false
    }).map((item: any) => {
      return item.id
    })

    this.manageService.materialService.deleteIcon({
      id: dels
    }).then((res: ResponseProps) => {
      if (res.status === 0) {
        this.refresh()
      } else {
        this.$message.error(res.msg || '图标删除失败')
      }
    })
  }

  // Capture Event
  public globalClick (e: any): void {
    if (this.lock) {
      e.preventDefault()
      e.stopPropagation()
    }
  }

  public search (): void {
    console.log('search')
  }

  public selectIcon (item: any): void {
    item.isChecked = !item.isChecked
  }

  public triggerEdit (e: any, item: any): void {
    e.stopPropagation()
    item.isEdit = true
    this.lock = true
    this.editingName = item.name
    e.target.focus()
  }

  public stopEdit (item: any) {
    this.lock = false
    item.isEdit = false
  }

  public editName (e: any, item: any): void {
    if (item.name.trim() === '') {
      this.$message.error('图标名不能为空')
      return
    }

    if (item.name.trim() === this.editingName) {
      this.stopEdit(item)
      item.name = this.editingName
      return
    }

    this.manageService.materialService
      .updateIcon({
        id: item.id,
        name: item.name
      }).then((res: any) => {
        if (res.status === 0) {
          this.stopEdit(item)
        } else {
          this.$message.error(res.msg)
        }
      })
  }

  public editBlur (e: any, item: any): void {
    if (item.name.trim() === '') {
      e.target.focus()
      this.$message.error('图标名不能为空')
      return
    }

    if (item.isEdit) {
      e.target.focus()
      this.editName(e, item)
    }
  }

  public beforeUpload (file: any): boolean {
    const fileType = file.type
    if (!this.utils.isImgType(fileType)) {
      this.$message.error('图片格式错误')
      return false
    }
    if (file.size / 1024 / 1024 > 2) {
      this.$message.error('图片超出大小限制')
      return false
    }
    return true
  }

  public uploadSuccess (res: any, file: File, fileList: File[]) {
    this.$message.success('上传成功')
    this.refresh()
  }

  public uploadError (res: any, file: File, fileList: File[]) {
    this.$message.error(res.msg || '上传失败')
  }
}

class SearchParams {
  public search_content: string
  public page_no: number
  public page_size: number

  constructor (
    search_content: string = '',
    page_no: number = 1,
    page_size: number = 10) {
    [
      this.search_content,
      this.page_no,
      this.page_size
    ] = [
      search_content,
      page_no,
      page_size
    ]
  }
}
