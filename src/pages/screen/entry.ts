import { Vue, Component, Prop, Watch } from 'vue-property-decorator'
import { Action, Getter, State } from 'vuex-class'
import screenDefault from '@/assets/imgs/screen-default.png'

@Component
export default class Screen extends Vue {
  public screenList: any[] = []
  public screenDefaultImg: string = screenDefault
  public resourceUrl: string = ''
  public isFullScreen: boolean = false

  public created (): void {
    this.resourceUrl = localStorage.getItem('resource_url') as string || ''
    if (
      this.resourceUrl.trim() !== '' &&
      !this.resourceUrl.startsWith('http://') &&
      !this.resourceUrl.startsWith('https://')
    ) {
      this.resourceUrl = `http://${this.resourceUrl}`
    }
    this.getScreenList()
  }

  public changeScreenMode (): void {
    this.isFullScreen = !this.isFullScreen
    this.isFullScreen ? this.requestFullscreen() : this.exitFullscreen()
  }

  public requestFullscreen (): void {
    const el: any = document.documentElement
    const rfs = el.requestFullScreen || el.webkitRequestFullScreen ||
        el.mozRequestFullScreen || el.msRequestFullScreen
    rfs.call(el)
  }

  public exitFullscreen (): void {
    const el: any = document
    const cfs = el.cancelFullScreen || el.webkitCancelFullScreen ||
        el.mozCancelFullScreen || el.exitFullScreen
    cfs.call(el)
  }

  public getScreenList (): void {
    this.manageService.menuService.getMenuList({
      level: 3,
      parent_menu_id: this.$route.params.id
    }).then((res: any) => {
      if (res.status === 0) {
        this.screenList = res.data.list
        // this.getImage()
      } else {
        this.$message({
          message: res.msg,
          type: 'error'
        })
      }
    })
  }

  public getImage (): void {
    this.screenList.forEach((item: any, index: number) => {
      item.image = `/api/menu/image?id=${item.id}`
    })
  }

  public changeScreenView (resourceUrl: string): void {
    this.resourceUrl = resourceUrl
  }
}
