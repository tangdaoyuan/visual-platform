import { Vue, Component, Prop, Watch } from 'vue-property-decorator'
import { Action, Getter, State } from 'vuex-class'

@Component
export default class ModifyPassword extends Vue {
  @Getter('user/userInfo') public userInfo!: any
  @Prop({ type: Boolean,default: false }) public value !: boolean

  public validatePass = (rule: any, value: string, callback: any) => {
    if (value === '') {
      callback(new Error('请再次输入密码'))
    } else if (value !== this.passwordForm.new_password) {
      callback(new Error('两次输入密码不一致!'))
    } else {
      callback()
    }
  }
  public passwordForm: PasswordForm = new PasswordForm()
  public passwordRule: PasswordRule = new PasswordRule(this.validatePass)


  public submitForm (formName: any): void {
    const form: any = this.$refs[formName]
    form.validate((valid: boolean) => {
      if (valid) {
        this.modifyPassword(this.passwordForm)
      }
    })
  }

  public modifyPassword (passwordForm: any): void {
    this.loginService.changePasswword(passwordForm).then((res: any) => {
      if (res.status === 0) {
        this.$message({
          message: '修改成功',
          type: 'success'
        })
        this.close()
      } else {
        this.$message({
          message: res.msg,
          type: 'error'
        })
      }
    })
  }

  public close (): void {
    const form: any = this.$refs.passwordForm
    form.resetFields()
    this.$emit('close')
  }

}

class PasswordForm {
  public username: string = ''
  public old_password: string = ''
  public new_password: string = ''
  public confirm_password: string = ''

  constructor () {
    [this.username, this.old_password, this.new_password,this.confirm_password] = ['', '', '', '']
  }
}

class Rule {
  public required: boolean
  public trigger: string
  public message: string

  constructor (type: string, required: boolean = true, trigger: string = 'blur') {
    let message: string
    {
      message = (() => {
        switch (type) {
          case 'username':
            return '请输入用户名'
          case 'old_password':
            return '请输入原始密码'
          case 'new_password':
            return '请输入新密码'
          default:
            return '请输入'
        }
      })()
    }
    [
      this.required,
      this.trigger,
      this.message
    ] = [
      required,
      trigger,
      message
    ]
  }
}

class SelfRule {
  public required: boolean
  public validator: any
  public trigger: string

  constructor (validator: any,trigger: string = 'blur',required: boolean = true) {
    this.validator = validator
    this.trigger = trigger
    this.required = required
  }
}

class PasswordRule {
  public username: Rule
  public old_password: Rule
  public new_password: Rule
  public confirm_password: SelfRule

  constructor (validator: any) {
    [
      this.username,
      this.old_password,
      this.new_password,
      this.confirm_password
    ] = [
      new Rule('username'),
      new Rule('old_password'),
      new Rule('new_password'),
      new SelfRule(validator)
    ]
  }
}
