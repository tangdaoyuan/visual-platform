import { Vue, Component, Prop, Watch } from 'vue-property-decorator'
import { Action, Getter, Mutation } from 'vuex-class'
import ModifyPasswordModal from './components/modify_password_modal/index.vue'

@Component({
  components: {
    ModifyPasswordModal
  }
})
export default class Login extends Vue {
  @Action('user/setUserInfo') public setUserInfo: any
  @Action('user/setLoginStatus') public setLoginStatus: any

  public loginForm: LoginForm = new LoginForm()
  public loginRule: LoginRule = new LoginRule()
  public modals: any = {
    modifyPassword: false
  }

  public login (): void {
    this.loginService.login(this.loginForm).then((res: any) => {
      if (res.status === 0) {
        this.setUserInfo(res.data)
        this.setLoginStatus(true)
        this.$router.push({
          name: 'home'
        })
      } else {
        this.$message({
          message: res.msg,
          type: 'error'
        })
      }
    })
  }

  public submitForm (formName: any): void {
    const form: any = this.$refs[formName]
    form.validate((valid: boolean) => {
      if (valid) {
        this.login()
      }
    })
  }

  public closeModifyPasswordModal (): void {
    this.modals.modifyPassword = false
  }
}

class LoginForm {
  public username: string = ''
  public password: string = ''

  constructor () {
    [this.username, this.password] = ['', '']
  }
}

class Rule {
  public required: boolean
  public trigger: string
  public validator: any

  constructor (type: string, required: boolean = true, trigger: string = 'blur') {
    let message: string
    {
      message = (() => {
        switch (type) {
          case 'username':
            return '用户名不能为空'
          case 'password':
            return '密码不能为空'
          default:
            return '用户名不能为空'
        }
      })()
    }
    [
      this.required,
      this.trigger,
      this.validator
    ] = [
      required,
      trigger,
      (rule: any, value: string, callback: any) => {
        if (value === '') {
          callback(new Error(message))
        } else {
          callback()
        }
      }
    ]
  }
}

class LoginRule {
  public username: Rule
  public password: Rule

  constructor () {
    [
      this.username,
      this.password
    ] = [
      new Rule('username'),
      new Rule('password')
    ]
  }
}
