import { Vue, Component, Prop, Watch } from 'vue-property-decorator'
import { Action, Getter, State } from 'vuex-class'
import fileDefaultIcon from '@/assets/imgs/file-icon.png'
@Component
export default class AdminFileList extends Vue {
  @Getter('user/isAdmin') public isAdmin!: boolean

  public defaultIcon: string = fileDefaultIcon
  public isShowScreenList: boolean = false
  public folderList: any[] = []
  public currentScreenList: any[] = []
  public loading: any
  public isShowNoScreen: boolean = false

  public created (): void {
    this.isAdmin && this.getFileList({
      level: 1
    })
  }

  public getFileList (formData: object): void {
    const self = this
    this.manageService.menuService.getMenuList(formData).then((res: any) => {
      if (res.status === 0) {
        this.folderList = res.data.list.map((item: any) => {
          return {
            ...item,
            isActive: false
            // image : this.defaultIcon
          }
        })
        this.folderList = this._.chunk(this.folderList,12)
        // this.getImage()
      } else {
        this.$message({
          message: res.msg,
          type: 'error'
        })
      }
    })
  }

  public getImage (): void {
    this.folderList.forEach((chunkList: any[]) => {
      chunkList.forEach((item: any) => {
        const oImg = new Image()
        oImg.src = `/api/menu/image?id=${item.id}`
        oImg.onload = () => {
          item.image = oImg.src
        }
      })
    })
  }

  public getMenuTreeList (folder: any, index: number): void {
    this.manageService.menuService.getMenuTreeList({
      parent_menu_id: folder.id
    }).then((res: any) => {
      if (res.status === 0) {
        folder.children = res.data[0].children || []
        folder.isActive = true
      } else {
        this.$message({
          message: res.msg,
          type: 'error'
        })
      }
    })
  }

  public showSecFolder (folder: any, index: number): void {
    return // 客户不要该效果
    if (folder.children) {
      folder.isActive = true
    } else {
      this.getMenuTreeList(folder, index)
    }
  }

  public hideSecFolder (folder: any): void {
    folder.isActive = false
    this.currentScreenList = []
    this.isShowNoScreen = false
  }

  public showScreenList (secondFolder: any): void {
    this.currentScreenList = secondFolder.children || []
    this.isShowNoScreen = !this.currentScreenList.length
  }

  public showUserFileList (folder: any): void {
    this.$emit('showUserFileList', folder)
  }

  public toScreenView (screen: any): void {
    this.utils.viewGo(`/#/screen/${screen.parent_menu_id}`)
    sessionStorage.setItem('resource_url',screen.resource_url || '')
  }
}
