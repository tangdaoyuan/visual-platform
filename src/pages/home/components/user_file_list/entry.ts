import { Vue, Component, Prop, Watch } from 'vue-property-decorator'
import { Action, Getter, State } from 'vuex-class'
import screenDefault from '@/assets/imgs/screen-default.png'

@Component
export default class UserFileList extends Vue {
  public currentFileIndex: number = -1
  public currentParentName: string = ''
  public screenDefaultImg: string = screenDefault
  public menuTreeList: any[] = []

  @Prop({ type: String, default: '' }) public parentMeunId!: string
  @Prop({ type: String, default: '' }) public policeCategory!: string
  @Getter('user/isAdmin') public isAdmin!: boolean
  @Getter('user/userInfo') public userInfo!: any

  @Watch('parentMeunId')
  public onParentMeunIdChanged (val: string, oldVal: string) {
    if (val && val !== oldVal) {
      this.getMenuTreeList()
    }
  }

  public created (): void {
    !this.isAdmin && this.getMenuTreeList()
  }

  public mounted (): void {
    this.addIndicatorEvent()
  }

  public beforeDestroy (): void {
    this.$('.file-carousel > ul').off('mouseover mouseout', 'li')
  }

  public addIndicatorEvent (): void {
    const self = this
    this.$('.file-carousel > ul').on('mouseover','li', (e: any) => {
      const target = e.currentTarget
      const index = self.$(target).index()
      if (target.lastChild.id === 'fileNameTip') {
        target.lastChild.style.display = 'inline'
      } else {
        const fileNameTipDom = document.createElement('span')
        fileNameTipDom.innerText = this.menuTreeList[index].name
        fileNameTipDom.id = 'fileNameTip'
        fileNameTipDom.className = 'fileNameTip'
        fileNameTipDom.style.position = 'fixed'
        const x = self.$(target).offset().left
        const y = self.$(target).offset().top
        const len = fileNameTipDom.innerText.length
        fileNameTipDom.style.left = x - 3 * len + 'px'
        fileNameTipDom.style.top = y + 18 + 'px'
        target.append(fileNameTipDom)
      }
    })
    this.$('.file-carousel > ul').on('mouseout','li', (e: any) => {
      const target: any = e.currentTarget
      if (target.lastChild.id === 'fileNameTip') {
        target.lastChild.style.display = 'none'
      }
    })
  }

  public getMenuTreeList (): void {
    const params = this.isAdmin ? { parent_menu_id: this.parentMeunId } : { level: 2 }
    this.manageService.menuService.getMenuTreeList(params).then((res: any) => {
      if (res.status === 0) {
        const menuTreeList = this.isAdmin ? res.data[0].children || [] : res.data
        this.menuTreeList = menuTreeList.map((item: any) => {
          let children: any[] = []
          if (item.children) {
            children = this._.chunk(item.children,12)
          }
          return { ...item, children }
        })
        this.currentParentName = this.menuTreeList.length > 0 ?
          this.menuTreeList[0].parent_menu_name : ''
      } else {
        this.$message({
          message: res.msg,
          type: 'error'
        })
      }
    })
  }

  public changeFile (index: number): void {
    this.currentFileIndex = index
    this.currentParentName = this.menuTreeList[index].parent_menu_name
  }

  public toScreenView (screen: any): void {
    this.utils.viewGo(`/#/screen/${screen.parent_menu_id}`)
    localStorage.setItem('resource_url',screen.resource_url || '')
  }
}
