import { Vue, Component, Prop, Watch } from 'vue-property-decorator'
import { Action, Getter, State } from 'vuex-class'

@Component
export default class AddDemand extends Vue {
  @Getter('user/userInfo') public userInfo!: any
  @Prop({ type: Boolean,default: false }) public value !: boolean
  @Prop({ type: String,default: '' }) public id !: string
  @Prop({ type: Object,default: {} }) public detail !: any
  @Watch('value')
  public onVisable (val: any, oldVal: any) {
    if (val && this.id) {
      const { name ,describe, priority } = this.detail
      this.demandForm = new DemandForm(name, describe, priority)
    }
  }
  public demandForm: DemandForm = new DemandForm()
  public demandRule: DemandRule = new DemandRule()
  public currentTime: string = ''

  public created (): void {
    this.currentTime = this.utils.dateFormat(new Date(),'yyyy-MM-dd')
  }

  public submitForm (formName: any): void {
    const form: any = this.$refs[formName]
    form.validate((valid: boolean) => {
      if (valid) {
        this.id ? this.editDamand() : this.addDamand()
      }
    })
  }

  public addDamand (): void {
    this.manageService.demandService.saveDemand(this.demandForm).then((res: any) => {
      if (res.status === 0) {
        this.$message({
          message: '提交成功',
          type: 'success'
        })
        this.close()
      } else {
        this.$message({
          message: res.msg,
          type: 'error'
        })
      }
    })
  }

  public editDamand (): void {
    this.manageService.demandService.updateNeeds({
      ...this.demandForm,
      id: this.id
    }).then((res: any) => {
      if (res.status === 0) {
        this.$message({
          message: '提交成功',
          type: 'success'
        })
        this.close()
        this.$emit('editSuccess')
      } else {
        this.$message({
          message: res.msg,
          type: 'error'
        })
      }
    })
  }

  public close (): void {
    // const form: any = this.$refs.demandForm
    // form.resetFields()
    this.demandForm = new DemandForm()
    this.$emit('close')
  }

}

class DemandForm {
  public name: string
  public describe: string
  public priority: number

  constructor (name: string = '', describe: string = '', priority: number = 3) {
    [ this.name, this.describe, this.priority] = [ name, describe, priority]
  }
}

class Rule {
  public required: boolean
  public trigger: string
  public message: string

  constructor (type: string, required: boolean = true, trigger: string = 'blur') {
    let message: string
    {
      message = (() => {
        switch (type) {
          case 'name':
            return '请输入需求名称'
          case 'describe':
            return '请输入需求描述'
          case 'priority':
            return '请选择紧急状态'
          default:
            return '请输入'
        }
      })()
    }
    [
      this.required,
      this.trigger,
      this.message
    ] = [
      required,
      trigger,
      message
    ]
  }
}

class DemandRule {
  public name: Rule
  public describe: Rule
  public priority: Rule

  constructor () {
    [
      this.name,
      this.describe,
      this.priority
    ] = [
      new Rule('name'),
      new Rule('describe'),
      new Rule('priority')
    ]
  }
}
