import { Vue, Component, Prop, Watch } from 'vue-property-decorator'
import { Action, Getter, State } from 'vuex-class'

@Component
export default class DemandList extends Vue {
  @Prop({ type: Boolean,default: false }) public value !: boolean
  @Watch('value')
  public visible () {
    if (this.value) {
      this.init()
    }
  }

  public tableData: any[] = []

  public reasonModal: boolean = false

  public modals: ModalGroup = new ModalGroup()

  public reasons: any[] = []

  public init (): void {
    this.getDemandList()
  }

  public getDemandList () {
    this.manageService.demandService.getDemandList({}).then((res: any) => {
      if (res.status === 0) {
        this.tableData = res.data.list.map((item: any) => {
          return {
            ...item,
            visible : false
          }
        })
      } else {
        this.$message({
          message: res.msg,
          type: 'error'
        })
      }
    })
  }

  public reason (id: string) {
    this.manageService.demandService.getDemandDetail({
      id
    }).then((res: any) => {
      if (res.status === 0) {
        this.reasons = res.data.reject_record_list
        this.modals.showReasonModal()
      }
    })
  }

  public close (): void {
    this.$emit('close')
  }

  public handleRevoke (row: any): void {
    row.visible = false
    this.manageService.demandService.revokeDemand({
      id: row.id
    }).then((res: any) => {
      if (res.status === 0) {
        this.$message({
          message: '撤回成功',
          type: 'success'
        })
        this.getDemandList()
      } else {
        this.$message({
          message: res.msg,
          type: 'error'
        })
      }
    })
  }

  public submitDemand (id: string): void {
    this.manageService.demandService.updateDemand({
      id,
      status: this.CONSTANT.demandStatus.UNCHECK
    }).then((res: any) => {
      if (res.status === 0) {
        this.$message({
          message: '提交成功',
          type: 'success'
        })
        this.getDemandList()
      } else {
        this.$message({
          message: res.msg,
          type: 'error'
        })
      }
    })
  }

  public editDemand (row: any): void {
    this.$emit('edit', row)
  }
}

class ModalGroup {
  public reasonModal: boolean

  constructor () {
    [
      this.reasonModal
    ] = [
      false
    ]
  }

  public showReasonModal = (): void => {
    this.reasonModal = true
  }

  public closeReasonModal = (): void => {
    this.reasonModal = false
  }
}
