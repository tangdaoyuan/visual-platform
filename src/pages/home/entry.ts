import { Vue, Component, Prop, Watch } from 'vue-property-decorator'
import { Action, Getter, Mutation } from 'vuex-class'
import userIcon from '@/assets/imgs/user-icon.png'
import AddDemandModal from './components/add_demand_modal/index.vue'
import DemandListModal from './components/demand_list_modal/index.vue'
import AdminFileList from './components/admin_file_list/index.vue'
import UserFileList from './components/user_file_list/index.vue'

@Component({
  components: {
    AddDemandModal,
    DemandListModal,
    AdminFileList,
    UserFileList
  }
})
export default class Home extends Vue {
  @Getter('user/userInfo') public userInfo!: any
  @Getter('user/isAdmin') public isAdmin!: boolean
  @Getter('home/adminFileViewStatus') public adminFileViewStatus!: string
  @Action('user/setLoginStatus') public setLoginStatus: any
  @Action('home/setAdminFileViewStatus') public setAdminFileViewStatus: any

  public defaultIcon: string = userIcon
  public isShowUserFileList: boolean = false
  public menuId: string = ''
  public policeCategory: string = ''
  public modals: ModalGroup = new ModalGroup()
  public demandDetail: any = {}

  public created (): void {
    this.setAdminFileViewStatus(this.isAdmin ? 'admin' : 'user')
  }

  public toManage (): void {
    this.utils.viewGo('/#/manage')
  }

  public showUserFileList (folder: any): void {
    this.setAdminFileViewStatus('user')
    this.menuId = folder.id
    this.policeCategory = folder.name
  }

  public showAdminFileList (): void {
    this.setAdminFileViewStatus('admin')
  }

  public logout (): void {
    this.loginService.logout({}).then((res: any) => {
      if (res.status === 0) {
        this.setLoginStatus(false)
        this.$router.push({
          name: 'login'
        })
      } else {
        this.$message({
          message: res.msg,
          type: 'error'
        })
      }
    })
  }

  public editDemand (row: any): void {
    this.demandDetail = row
    this.modals.showAddDemandModal()
  }

  public editSuccess (): void {
    const demandList: any = this.$refs.demandList
    demandList.getDemandList()
  }
}

class ModalGroup {
  public addDemand: boolean
  public demandList: boolean

  constructor () {
    [
      this.addDemand,
      this.demandList
    ] = [
      false,
      false
    ]
  }

  public showAddDemandModal = (): void => {
    this.addDemand = true
  }

  public closeAddDemandModal = (): void => {
    this.addDemand = false
  }

  public showDemandListModal = (): void => {
    this.demandList = true
  }

  public closeDemandListModal = (): void => {
    this.demandList = false
  }
}
