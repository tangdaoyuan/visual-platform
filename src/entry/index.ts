import { Component, Vue } from 'vue-property-decorator'
import { Getter } from 'vuex-class'


@Component({})
export default class App extends Vue {
  @Getter('user/isLogin') public isLogin !: boolean

  public created (): void {
    if (!this.isLogin) {
      this.$router.push({
        name: 'login'
      })
    }
  }
}
