import Service from '..'

export class HomeService extends Service {
  constructor (path: string = '/api/home') {
    super(path)
  }
}

export default new HomeService()
