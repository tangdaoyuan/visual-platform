import Vue from 'vue'
import Axios from 'axios'
import store from '@/stores'
import { Message, Loading } from 'element-ui'
import Cookie from 'js-cookie'
import router from '../routers/router'

interface HttpBase {
  http: any,
  get (str: string, data: object, resolve: any): void,
  delete (str: string, data: object, resolve: any): void,
  put (str: string, data: object, resolve: any): void,
  post (str: string, data: object, resolve: any): void
}

class AxiosUtil {
  private static axios: any = null
  private static reqCount: number = 0
  private static loadingInstance: any = null
  public static getInstance (): any {
    if (!this.axios) {
      this.axios = Axios
      this.axios.timeout = 45000
      this.axios.interceptors.request.use((config: any) => {
        if (store.getters['user/isLogin'] && Cookie.get('user_info')) {
          const user: any = JSON.parse(Cookie.get('user_info') as string)
          config.headers.Authorization = `${user.token_type} ${user.access_token}`
        }
        if (this.reqCount === 0) {
          this.loadingInstance = Loading.service({
            lock: true,
            fullscreen: true,
            background: 'rgba(0, 0, 0, 0.7)'
          })
        }
        this.reqCount++
        return config
      }, (error: any) => {
        this.reqCount--
        if (this.reqCount === 0) {
          Vue.nextTick(() => {
            this.loadingInstance.close()
          })
        }
        return Promise.reject(error)
      })
      this.axios.interceptors.response.use((response: any) => {
        this.reqCount--
        if (this.reqCount === 0) {
          Vue.nextTick(() => {
            this.loadingInstance.close()
          })
        }
        return response
      }, (error: any) => {
        if (error.response) {
          this.reqCount--
          if (this.reqCount === 0) {
            Vue.nextTick(() => {
              this.loadingInstance.close()
            })
          }
          const status = error.response.status
          if (status === 401 || status === 403) {
            Message({
              message: '登录已经失效，请重新登录',
              type: 'error'
            })
            store.dispatch('user/setLoginStatus',false)
            router.replace({
              path: '/login'
            })
          }
        }
        return Promise.reject(error)
      })
    }
    return this.axios
  }
}

export default class BaseService implements HttpBase {
  public http: any = null
  public ROOT_URL !: string
  constructor (path: string) {
    this.http = AxiosUtil.getInstance()
    this.ROOT_URL = path
  }

  public get (str: string, data: object, resolve: any): void {
    this.http.get(`${this.ROOT_URL}${str}`, {
      params: data || {}
    }).then((res: any) => {
      if (res.status === 200 && res.data.status === 0) {
        resolve(res.data)
      } else {
        resolve(res.data || {
          status: 1,
          msg: '请求失败'
        })
      }
    })
  }

  public delete (str: string, data: object, resolve: any, isJson: boolean = false): void {
    const PARAMS: any = {}
    if (!isJson) {
      PARAMS.params = data || {}
    } else {
      PARAMS.data = data || {}
    }

    this.http.delete(`${this.ROOT_URL}${str}`, PARAMS).then((res: any) => {
      if (res.status === 200 && res.data.status === 0) {
        resolve(res.data)
      } else {
        resolve(res.data || {
          status: 1,
          msg: '请求失败'
        })
      }
    })
  }

  public put (str: string, data: object, resolve: any): void {
    this.http.put(`${this.ROOT_URL}${str}`, data).then((res: any) => {
      if (res.status === 200 && res.data.status === 0) {
        resolve(res.data)
      } else {
        resolve(res.data || {
          status: 1,
          msg: '请求失败'
        })
      }
    })
  }

  public post (str: string, data: object, resolve: any, headers: any = {}): void {
    this.http.post(`${this.ROOT_URL}${str}`, data, headers).then((res: any) => {
      if (res.status === 200 && res.data.status === 0) {
        resolve(res.data)
      } else {
        resolve(res.data || {
          status: 1,
          msg: '请求失败'
        })
      }
    })
  }

}

const services: any = {}

const files = require.context('./', true, /\.ts$/)

files.keys().forEach((item: string) => {
  if (item.indexOf('index') !== -1) {
    if (item.indexOf('./index') === -1) {
      const tmpKey: string = item.replace(/\.\//g, '').replace(/\/index/g, '').replace(/\.ts/g, 'Service')
      services[tmpKey] = files(item).default
    }
  }
})

export {
  services
}
