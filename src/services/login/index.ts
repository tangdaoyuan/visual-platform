import Service from '..'

interface LoginBase<T> {
  login (data: object): Promise<T>
  getUserInfo (data: object): Promise<T>
  logout (data: object): Promise<T>
}

export class LoginService extends Service implements LoginBase<any> {
  constructor (path: string = '/api/account/user') {
    super(path)
  }
  public login (data: object): Promise<any> {
    return new Promise((resolve: any) => {
      super.post('/login', data, resolve)
    })
  }
  public getUserInfo (data: object): Promise<any> {
    return new Promise((resolve: any) => {
      super.get('/profile', data, resolve)
    })
  }
  public logout (data: object): Promise<any> {
    return new Promise((resolve: any) => {
      super.put('/logout', data, resolve)
    })
  }
  public changePasswword (data: object): Promise<any> {
    return new Promise((resolve: any) => {
      super.get('/change_pwd', data, resolve)
    })
  }
}

export default new LoginService()
