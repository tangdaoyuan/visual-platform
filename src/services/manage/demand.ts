import Service from '..'

interface DemandBase<T> {
  getDemandList (data: object): Promise<T>
  getDemandDetail (data: object): Promise<T>
  saveDemand (data: object): Promise<T>
  deleteDemand (data: object): Promise<T>
  updateDemand (data: object): Promise<T>
}

class DemandService extends Service implements DemandBase<any> {
  constructor (path: string = '/api/needs') {
    super(path)
  }
  public getDemandList (data: object): Promise<any> {
    return new Promise((resolve) => {
      super.get('/list', data, resolve)
    })
  }
  public getDemandDetail (data: object): Promise<any> {
    return new Promise((resolve) => {
      super.get('/detail', data, resolve)
    })
  }
  public saveDemand (data: object): Promise<any> {
    return new Promise((resolve) => {
      super.post('/save', data, resolve)
    })
  }
  public deleteDemand (data: object): Promise<any> {
    return new Promise((resolve) => {
      super.delete('/delete', data, resolve)
    })
  }
  public updateDemand (data: object): Promise<any> {
    return new Promise((resolve) => {
      super.post('/update_status', data, resolve)
    })
  }
  public revokeDemand (data: object): Promise<any> {
    return new Promise((resolve) => {
      super.get('/revoke', data, resolve)
    })
  }
  public updateNeeds (data: object): Promise<any> {
    return new Promise((resolve) => {
      super.post('/update_needs', data, resolve)
    })
  }
}

export default DemandService
