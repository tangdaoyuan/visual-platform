import Service from '..'

interface StaticBase<T> {
  image (resourceUrl: string): Promise<T>
}

class StaticService extends Service implements StaticBase<any> {
  constructor (path: string = '') {
    super(path)
  }

  public image (resourceUrl: string): Promise<any> {
    return new Promise((resolve: any) => {
      this.http.get(resourceUrl, {
        responseType: 'arraybuffer',
        params: {}
      }).then((res: any) => {
        if (res.status === 200) {
          const blob = new Blob(
            [ res.data ],
            { type: res.headers['content-type'] }
          )
          resolve(blob)
        }
      })
    })
  }
}

export default StaticService
