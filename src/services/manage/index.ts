import MenuService from './menu'
import DemandService from './demand'
import RoleService from './role'
import UserService from './user'
import StaticService from './static'
import MaterialService from './material'

export class ManageService {
  private _menuService !: MenuService
  private _demandService !: DemandService
  private _roleService !: RoleService
  private _userService !: UserService
  private _staticService !: StaticService
  private _materialService !: MaterialService

  public get menuService (): MenuService {
    if (!this._menuService) this._menuService = new MenuService()
    return this._menuService
  }

  public get demandService (): DemandService {
    if (!this._demandService) this._demandService = new DemandService()
    return this._demandService
  }

  public get roleService (): RoleService {
    if (!this._roleService) this._roleService = new RoleService()
    return this._roleService
  }

  public get userService (): UserService {
    if (!this._userService) this._userService = new UserService()
    return this._userService
  }

  public get staticService (): StaticService {
    if (!this._staticService) this._staticService = new StaticService()
    return this._staticService
  }

  public get materialService (): MaterialService {
    if (!this._materialService) this._materialService = new MaterialService()
    return this._materialService
  }
}

export default new ManageService()
