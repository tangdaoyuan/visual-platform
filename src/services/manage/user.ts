import Service from '..'

interface UserBase<T> {
  getUserList (data: object): Promise<T>
  createUser (data: object): Promise<T>
  updateUser (data: object): Promise<T>
  removeUser (data: object): Promise<T>
  resetPwd (data: object): Promise<T>
  getUserDetail (data: object): Promise<T>
}

class UserService extends Service implements UserBase<any> {
  constructor (path: string = '/api/account/user') {
    super(path)
  }

  public getUserList (data: object): Promise<any> {
    return new Promise((resolve: any) => {
      super.get('/list', data, resolve)
    })
  }

  public createUser (data: object): Promise<any> {
    return new Promise((resolve: any) => {
      super.post('/register', data, resolve)
    })
  }

  public updateUser (data: object): Promise<any> {
    return new Promise((resolve: any) => {
      super.put('/update', data, resolve)
    })
  }

  public removeUser (data: object): Promise<any> {
    return new Promise((resolve: any) => {
      super.delete('/delete', data, resolve)
    })
  }

  public resetPwd (data: object): Promise<any> {
    return new Promise((resolve: any) => {
      super.get('/reset_other_pwd', data, resolve)
    })
  }

  public getUserDetail (data: object): Promise<any> {
    return new Promise((resolve: any) => {
      super.get('/profile', data, resolve)
    })
  }
}

export default UserService
