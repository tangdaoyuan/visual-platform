import Service from '..'

interface RoleBase<T> {
  getRoleList (data: object): Promise<T>
  getRoleDetail (data: object): Promise<T>
  saveRole (data: object): Promise<T>
  saveRole2Menu (data: object): Promise<T>
  saveRole2User (data: object): Promise<T>
  deleteRole (data: object): Promise<T>
}

class RoleService extends Service implements RoleBase<any> {
  constructor (path: string = '/api/role') {
    super(path)
  }
  public getRoleList (data: object): Promise<any> {
    return new Promise((resolve) => {
      super.get('/list', data, resolve)
    })
  }
  public getRoleDetail (data: object): Promise<any> {
    return new Promise((resolve) => {
      super.get('/item', data, resolve)
    })
  }
  public saveRole (data: object): Promise<any> {
    return new Promise((resolve) => {
      super.post('/save', data, resolve)
    })
  }
  public deleteRole (data: object): Promise<any> {
    return new Promise((resolve) => {
      super.delete('/delete', data, resolve)
    })
  }
  public saveRole2Menu (data: object): Promise<any> {
    return new Promise((resolve) => {
      super.post('/menu', data, resolve)
    })
  }
  public saveRole2User (data: object): Promise<any> {
    return new Promise((resolve) => {
      super.post('/user', data, resolve)
    })
  }
}

export default RoleService
