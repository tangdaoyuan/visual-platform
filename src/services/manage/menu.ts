import Service from '..'

interface MenuBase<T> {
  getMenuList (data: object): Promise<T>
  getMenuTreeList (data: object): Promise<T>
  getMenuDetail (data: object): Promise<T>
  deleteMenu (data: object): Promise<T>,
  saveMenu (data: object): Promise<T>,
}

class MenuService extends Service implements MenuBase<any> {
  constructor (path: string = '/api/menu') {
    super(path)
  }

  public getMenuList (data: object): Promise<any> {
    return new Promise((resolve: any) => {
      super.get('/list', data, resolve)
    })
  }

  public getMenuTreeList (data: object): Promise<any> {
    return new Promise((resolve) => {
      super.get('/tree_list', data, resolve)
    })
  }

  public getMenuDetail (data: object): Promise<any> {
    return new Promise((resolve: any) => {
      super.get('/detail', data, resolve)
    })
  }

  public deleteMenu (data: object): Promise<any> {
    return new Promise((resolve) => {
      super.delete('/delete', data, resolve)
    })
  }

  public saveMenu (data: object): Promise<any> {
    return new Promise((resolve) => {
      super.post('/save', data, resolve, {
        headers: { 'Content-Type': 'multipart/form-data' }
      })
    })
  }

  public sortMenu (data: object): Promise<any> {
    return new Promise((resolve) => {
      super.get('/sort', data, resolve)
    })
  }
}

export default MenuService
