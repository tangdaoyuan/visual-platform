import Service from '..'

interface MaterialBase<T> {
  icons (data: object): Promise<T>
  updateIcon (data: object): Promise<T>
  deleteIcon (data: object): Promise<T>
}

class MaterialService extends Service implements MaterialBase<ResponseProps> {
  constructor (path: string = '/api/icon') {
    super(path)
  }

  public icons (data: object): Promise<ResponseProps> {
    return new Promise((resolve: any) => {
      super.get('/list', data, resolve)
    })
  }

  public updateIcon (data: object): Promise<ResponseProps> {
    return new Promise((resolve: any) => {
      super.put('/update', data, resolve)
    })
  }

  public deleteIcon (data: object): Promise<ResponseProps> {
    return new Promise((resolve: any) => {
      super.delete('/delete', data, resolve, true)
    })
  }
}

export default MaterialService
