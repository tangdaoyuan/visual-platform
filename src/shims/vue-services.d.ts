import Vue from 'vue'
import { HomeService } from '../services/home'
import { ManageService } from '../services/manage'
import { LoginService } from '../services/login'

declare module 'vue/types/vue' {
  interface Vue {
    homeService: HomeService,
    manageService: ManageService,
    loginService: LoginService,
    process: any
  }
}
