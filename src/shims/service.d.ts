interface ResponseProps {
  status: number
  msg: string
  data: any | undefined
}
