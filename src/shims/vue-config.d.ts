
import Vue from 'vue'
import VueRouter from 'vue-router'
import { Route } from 'vue-router'
import { CONSTANT } from '@/constants/beans'

declare module 'vue/types/vue' {
  interface Vue {
    $router: VueRouter,
    $route: Route,
    _: any,
    $: any,
    echarts: any,
    cookies: any,
    utils: any,
    CONSTANT: CONSTANT
  }
}
