import Vue from 'vue'
import Router from 'vue-router'

import Login from '@/pages/login/index.vue'
import Home from '@/pages/home/index.vue'
import ScreenView from '@/pages/screen/index.vue'
import Manage from '@/pages/manage/index.vue'
import MenuManage from '@/pages/manage/menu_resource/index.vue'
import TopMenu from '@/pages/manage/menu_resource/top_menu/index.vue'
import SubMenu from '@/pages/manage/menu_resource/sub_menu/index.vue'
import ScreenContent from '@/pages/manage/menu_resource/screen_content/index.vue'
import ScreenDemand from '@/pages/manage/menu_resource/screen_demand/index.vue'
import RoleManage from '@/pages/manage/role/index.vue'
import UserManage from '@/pages/manage/user/index.vue'
import MaterialManage from '@/pages/manage/material/index.vue'

Vue.use(Router)

export default new Router({
  // mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/manage',
      name: 'manage',
      component: Manage,
      children: [
        {
          path: 'menu',
          name: 'menuManage',
          component: MenuManage,
          children: [
            {
              path: 'topmenu',
              name: 'topMenu',
              component: TopMenu
            },
            {
              path: 'submenu',
              name: 'subMenu',
              component: SubMenu
            },
            {
              path: 'content',
              name: 'screenContent',
              component: ScreenContent
            },
            {
              path: 'demand',
              name: 'screenDemand',
              component: ScreenDemand
            }
          ]
        },
        {
          path: '/role',
          name: 'roleManage',
          component: RoleManage
        },
        {
          path: '/user',
          name: 'userManage',
          component: UserManage
        },
        {
          path: '/material',
          name: 'materialManage',
          component: MaterialManage
        }
      ]
    },
    {
      path: '/',
      redirect: '/home'
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/home',
      name: 'home',
      component: Home
    },
    {
      path: '/screen/:id',
      name: 'screen',
      component: ScreenView
    }
  ]
})
