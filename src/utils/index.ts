import $ from 'jquery'
import store from '@/stores'
import Cookie from 'js-cookie'

class Utils {

  public viewGo (url: string): void {
    const a = $(`<a href="${url}" target="_blank"></a>`)
    const dom = a.get(0)
    const e = document.createEvent('MouseEvents')

    e.initEvent('click', true, true)
    dom.dispatchEvent(e)
    a.remove()
  }

  public authHeader (): any {
    if (store.getters['user/isLogin']) {
      const user: any = JSON.parse(Cookie.get('user_info') as string)
      return { Authorization: `${user.token_type} ${user.access_token}` }
    }
    return {}
  }

  public json2FormData (params: any): FormData {
    const formData = new FormData()
    Object.keys(params).forEach((key) => {
      if (params[key]) {
        formData.append(key, params[key])
      }
    })
    return formData
  }

  public isImgType (fileType: string) {
    // when filetype == image/*
    const types: string[] = fileType.split('/')
    if (types.length === 2 && types[0] === 'image') {
      const regx: RegExp = /^(png|jpeg|jpg|bmp|svg)$/
      return regx.test(types[1])
    }

    const test: RegExp = /.+\.(png|jpeg|jpg|bmp|svg)$/
    return test.test(fileType)
  }

  public recursiveMap (lists: any[], level: number): any {
    return lists.map((item: any) => {
      if (item.children && item.children.length > 0) {
        item.children = this.recursiveMap(item.children, level + 1)
      }
      return {
        ...item,
        level
      }
    })
  }

  public dateFormat (date = new Date(), fmt = 'yyyy-MM-dd hh:mm:ss'): string {
    const o: any = {
      'M+': date.getMonth() + 1,
      'd+': date.getDate(),
      'h+': date.getHours(),
      'm+': date.getMinutes(),
      's+': date.getSeconds(),
      'q+': Math.floor((date.getMonth() + 3) / 3),
      'S': date.getMilliseconds()
    }
    if (/(y+)/.test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length))
    }
    for (const k in o) {
      if (new RegExp('(' + k + ')').test(fmt)) {
        fmt = fmt.replace(RegExp.$1, RegExp.$1.length === 1 ? o[k] : ('00' + o[k]).substr(('' + o[k]).length))
      }
    }
    return fmt
  }
}

export default new Utils()
