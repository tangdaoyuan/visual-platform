
const apiAddr = 'http://graph.policegap.cn:8099'
const staticAddr = 'http://graph.policegap.cn:8099'
// const apiAddr = 'http://192.168.1.93:8181'

module.exports = {
  '/file': {
    'target': staticAddr,
    'changeOrigin': true
  },
  '/api': {
    'target': apiAddr,
    'changeOrigin': true
  }
}
